#!/bin/bash
#
# neOCampus operation
#
# Deploying specific settings for board
#
# F.Thiebolt    jul.24  updated for ESP32 IDF new dirs architecture
# F.Thiebolt    may.23  updated comments
# F.Thiebolt    aug.22  update for 2.0.4 sdk with esp32 target only
#                       i.e does not apply to esp32 derivatives like esp32c3
# F.Thiebolt    aug.21  initial release
#

#set -x

# checks
[ $# -lt 1 ] && { echo -e "\n###ERROR missing base dir parameter !" >&2; exit 1; }
[ -d $1 ] || { echo -e "\n###ERROR non existing directory '$1'!" >&2; exit 1; }

echo -e "\t[addon] Copying additional libraries/headers files ..."

_done=0

#
# copying lwip + header(s)

# [header] opt.h
_localFile="opt.h"
if [ -f ${_localFile} ]; then
    # find file in hierarchy and replace it
    _target=$(find $1 -name ${_localFile})
    #_target="$1/include/lwip/lwip/src/include/lwip/${_localFile}"
    [ -f ${_target} ] || { echo -e "\n###ERROR: unable to find file '${_target}' ... aborting" >&2; exit 1; }
    mv ${_target} ${_target}.orig
    cp -af -L ${_localFile} ${_target}
    [ $? -ne 0 ] && { echo -e "\n###ERROR copying some files to '${_target}'" >&2; exit 1; }
    _done=1
    chmod a+r ${_target}
fi

# [libraries]
_targetLibDir="$1/lib"
[ -d ${_targetLibDir} ] || { echo -e "\n###ERROR: unable to find dir '${_targetLibDir}' ... aborting" >&2; exit 1; }
for _localLib in $(ls *.a 2>/dev/null); do
    cp -af -L ${_localLib} ${_targetLibDir}
    [ $? -ne 0 ] && { echo -e "\n###ERROR copying file '${_localLib}' to '${_targetLibDir}'" >&2; exit 1; }
    _done=1
    chmod a+r ${_targetLibDir}/${_localLib}
done

if [ ${_done} -eq 1 ]; then
    echo -e "\t[addon] successfully copied lwip+headers :)"
else
    echo -e "\t[addon] not found any additional files ..."
fi


#set +x

