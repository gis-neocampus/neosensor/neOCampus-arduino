#include "Arduino.h"
#include <WiFi.h>

#include <esp_wifi.h>

void setup(){
  Serial.begin(115200);
  delay(5000);

  WiFi.mode(WIFI_MODE_STA);
  
  // Variable to store the MAC address
  uint8_t baseMac[6];
  
  // Get MAC address of the WiFi station interface
  esp_wifi_get_mac(WIFI_IF_STA,baseMac);
  Serial.print("Station MAC: ");
  for (int i = 0; i < 5; i++) {
    Serial.printf("%02X:", baseMac[i]);
  }
  Serial.printf("%02X\n", baseMac[5]);
  
  // Get the MAC address of the Wi-Fi AP interface
  esp_wifi_get_mac(WIFI_IF_AP, baseMac);
  Serial.print("SoftAP MAC: ");
  for (int i = 0; i < 5; i++) {
    Serial.printf("%02X:", baseMac[i]);
  }
  Serial.printf("%02X\n", baseMac[5]);
}
 
void loop() {
  delay(1000);
}
