#!/bin/bash
#
# Simple GIT-PUSH helper script ;)
# ===
# Notes:
# ===
# TODO:
# - regex with tokens to get taken into account
# ===
# F.Thiebolt    jan.24  removed git commit check return value (e.g nothing mode to commit)
#                       corrected typo 'bibucket' --> 'bitbucket' !
#                       corrected check against *bitbucket* (instead of bitbucket*)
# F.Thiebolt    sep.23  adaptation to gitlab URLs
#                       now differentiating push origin URL :D
# F.Thiebolt    2016    initial release
#

_SCRIPTREV="240122"

#DEBUG=1

# Debug
if [ ${DEBUG:-0} -eq 1 ]; then
    exec &> >(tee -a /tmp/$(basename ${BASH_SOURCE[0]}).log)
    echo -e "\n--- $(date +"%d-%m-%Y %H:%M") --- $(basename ${BASH_SOURCE[0]}) ----------------------------------------------"
    echo -e "Nb input params = $#, \$1='$1', \$2='$2'"
    env
    echo -e ""
    # enable trace mode
    set -x
fi


echo -e "#\n#\tGIT-PUSH | push script revision ${_SCRIPTREV:-'unknown'}\n#"

if [ "$#" == "0" ]; then
    msg='update'
else
    msg="$@"
fi


# [dec.22] removed StrictHostKey checking
export GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

# https://serverfault.com/questions/417241/extract-repository-name-from-github-url-in-bash
re="^(https|git)(:\/\/|@)([^\/:]+)[\/:]([^\/:]+)\/(.+)(.git)*$"
if [[ $(git config --local remote.origin.url) =~ $re ]]; then    
    _protocol=${BASH_REMATCH[1]}
    _separator=${BASH_REMATCH[2]}
    _hostname=${BASH_REMATCH[3]}
    _user=${BASH_REMATCH[4]}
    _repo=${BASH_REMATCH[5]}
fi

_git_username=${GIT_DEPLOY_USERNAME}
_git_token=${GIT_DEPLOY_TOKEN}
_git_hostname=$(git config --local remote.origin.url|sed -e 's/[^/]*\/\/\([^@]*@\)\?\([^:/]*\).*/\2/')
#_git_hostname=${_hostname}
_git_path=${_repo}
_git_remote_url="https://${_git_hostname}/${_git_path}"
#_git_remote_url="https://${_git_username}:${_git_token}@${_git_hostname}/${_git_path}"

[ ${DEBUG:-0} -eq 1 ] && { env; }


#
# set remote url
# - bitbucket with ssh
# - gitlab with or without username specified
if [[ ${_protocol} == http* && ${_hostname} != *bitbucket* ]]; then
    if [[ ${_hostname} == gitlab* ]]; then
        # url is probably of type https://gitlab.irit.fr/gis-neocampus/datalake/docker_datalake
        # thus DO NOT CHANGE remote url !
        echo -e "Gitlab url detected '$(git config --local remote.origin.url)'\n\twithout user specified ... thus nothing to change!"
    else
        echo -e "\n\tset git push remote url to '${_git_remote_url}' ..."
        git remote set-url --push origin ${_git_remote_url}
        [ $? -ne 0 ] && { echo -e "\n###ERROR: unable to set GIT remote url for repository '${_git_remote_url}' !!" >&2; exit 1; }
    fi
fi


git add --all
res=$?
[ $res -ne 0 ] && { echo -e "\n### git add ERROR code '$res' from REPO '$(git config --local remote.origin.url)' !" >&2; exit 1; }
git commit -a -m "${msg}"
#res=$?
#[ $res -ne 0 ] && { echo -e "\n### git commit ERROR '$res' from REPO '$(git config --local remote.origin.url)' !" >&2; exit 1; }
git push
res=$?
[ $res -ne 0 ] && { echo -e "\n### git push ERROR '$res' from REPO '$(git config --local remote.origin.url)' !" >&2; exit 1; }


# Debug
if [ ${DEBUG:-0} -eq 1 ]; then
    echo -e "--- $(date +"%d-%m-%Y %H:%M") -----------------------------------------------------------\n"
    # disable trace mode
    set +x
fi


