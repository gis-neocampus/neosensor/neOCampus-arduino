# neOSensor V5 | PMMA enclosure #
_________________________________

** .wcvr files ** are from the CampusFab's laser cutter

DXF files have been edited through the 'LibreCAD' tool

## FILES ##
  - 

TO BE CONTINUED
 24K -rw-r--r-- 1 francois cloud  24K Oct  7  2021 first-run.plt
984K -rw-r--r-- 1 francois cloud 983K Dec 13  2021 first-run.wcvr
1,6M -rw-r--r-- 1 francois cloud 1,6M Dec 13  2021 large-plate.wcvr
 28K -rw-r--r-- 1 francois cloud  28K Dec 13  2021 neOSensor-sds011_flat.dxf
120K -rw-r--r-- 1 francois cloud 118K Dec 13  2021 neOSensor-sds011_flat.wcvr
 20K -rw-r--r-- 1 francois cloud  19K Jun 18  2021 neOSensor_IR.dxf
 20K -rw-r--r-- 1 francois cloud  20K Sep 28  2021 neOSensor_V2.1_back.dxf
 20K -rw-r--r-- 1 francois cloud  19K Sep 28  2021 neOSensor_V2.2_back.dxf
 48K -rw-r--r-- 1 francois cloud  45K Dec  9  2021 neOSensor_bureau.dxf
 32K -rw-r--r-- 1 francois cloud  32K Dec  7  2021 neOSensor_bureauV2_Bottom5mm.dxf
 40K -rw-r--r-- 1 francois cloud  38K Dec  7  2021 neOSensor_bureauV2_Top3mm.dxf
100K -rw-r--r-- 1 francois cloud  98K Dec 13  2021 neOSensor_bureau-run1.wcvr
 88K -rw-r--r-- 1 francois cloud  87K Apr  9  2021 neOSensor_logo.dxf
 20K -rw-r--r-- 1 francois cloud  18K Apr  9  2021 neOSensor_plain.dxf
1,5M -rw-r--r-- 1 francois cloud 1,5M Dec 13  2021 second-run.wcvr



