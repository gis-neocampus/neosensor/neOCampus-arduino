This directory holds the following:

- pcb-v5b.pro	--> main neOSensor board (esp32 30 pins based version) version 5B

- panelization files (4 designs in a single 100x100mm board)

To launch Kicad: #> kicad xxx.pro

